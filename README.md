## Soremuscle Tf Gan

This project is a tensorflow implementation of the original GAN paper (https://arxiv.org/abs/1406.2661). The algorithm has been kept close to the paper on purpose, several enhancement can be made to improve the model/training.


## Dependencies

To run the training you need `Python3` with the python package `tensorflow`/`tensorflow-gpu` and `numpy` (will be installed automatically with tensorflow).

You can install the python packages using `pip` (`sudo apt install python3-pip` on debian based linux), with the command :

```
pip install tensorflow 
```

(`tensorflow-gpu` is recommended if you have a CUDA capable GPU, for more information see https://www.tensorflow.org/install/)



## Usage

Training teh model :

```
python train.py --checkpoint path/to/desired/folder
```

Note : the checkpoint path will be created if needed. If a folder already exists it will be replaced.


## Notes

* Last activation for generator and discriminator is very important and can drastically change the training

* Learning rates can also drastically change the training dynamic (generator being crushed by the discriminator usually)

* Having the discriminator too far ahead of the generator at the begining can leads to some mode collapse (generator loss > 2).
   The generator will output a fewers variations (noise is less used).

* Using batch normalization in the discriminator is very hard, the result tends to be an gray average instead of sharp shapes.
