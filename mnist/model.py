# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.ops import math_ops
from tensorflow.python.training import moving_averages

from model_config import ModelConfig


def inference(
        generator_input,
        discriminator_input,
        is_training=False,
        batch_norm_training=False):
    """Model definition"""

    def batch_normalization(symbolic_input, is_training=True, batch_norm_training=False,
                            epsilon=0.001, decay=0.999, name=None):
        # Perform a batch normalization after a conv layer or a fc layer
        # gamma: a scale factor
        # beta: an offset
        # epsilon: the variance epsilon - a small float number to avoid dividing by 0

        inputs_shape = symbolic_input.get_shape()
        axis = list(range(len(inputs_shape) - 1))
        params_shape = inputs_shape[-1:]

        gamma_init = tf.ones_initializer()
        beta_init = tf.zeros_initializer()

        moving_mean_init = tf.zeros_initializer()
        moving_variance_init = tf.ones_initializer()

        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            gamma = tf.get_variable('gamma', shape=params_shape, initializer=gamma_init)
            beta = tf.get_variable('beta', shape=params_shape, initializer=beta_init)

            moving_mean = tf.get_variable('moving_mean', shape=params_shape,
                                          initializer=moving_mean_init, trainable=is_training)
            moving_variance = tf.get_variable('moving_var', shape=params_shape,
                                              initializer=moving_variance_init, trainable=is_training)

            if is_training:
                # Calculate the moments based on the individual batch.
                mean, variance = tf.nn.moments(symbolic_input, axis)

                update_moving_mean = moving_averages.assign_moving_average(
                    moving_mean, mean, decay)
                update_moving_variance = moving_averages.assign_moving_average(
                    moving_variance, variance, decay)
                control_inputs = [update_moving_mean, update_moving_variance]

                def training():
                    return tf.nn.batch_normalization(
                        symbolic_input, mean, variance, offset=beta, scale=gamma,
                        variance_epsilon=epsilon, name='batch_norm')

                def non_training():
                    return tf.nn.batch_normalization(
                        symbolic_input, moving_mean, moving_variance, offset=beta, scale=gamma,
                        variance_epsilon=epsilon, name='batch_norm_no_train')

                with tf.control_dependencies(control_inputs):
                    output = tf.cond(batch_norm_training, training, non_training)
            else:
                # Just use the moving_mean and moving_variance.
                output = tf.nn.batch_normalization(
                    symbolic_input, moving_mean, moving_variance, offset=beta, scale=gamma,
                    variance_epsilon=epsilon, name='batch_norm')

            return output

    def leaky_relu(features, alpha=0.2, name=None):
        """Compute the Leaky ReLU activation function.
        "Rectifier Nonlinearities Improve Neural Network Acoustic Models"
        AL Maas, AY Hannun, AY Ng - Proc. ICML, 2013
        http://web.stanford.edu/~awni/papers/relu_hybrid_icml2013_final.pdf
        Args:
            features: A `Tensor` representing preactivation values.
            alpha: Slope of the activation function at x < 0.
            name: A name for the operation (optional).
        Returns:
            The activation value.
        """
        with ops.name_scope(name, "LeakyRelu", [features, alpha]):
            features = ops.convert_to_tensor(features, name="features")
            alpha = ops.convert_to_tensor(alpha, name="alpha")
        return math_ops.maximum(alpha * features, features)

    def convolution(symbolic_input, input_features, output_features, name="Conv", is_training=False, batch_norm=True,
                    inst_norm=False, kernel_size=3, strides=[1, 1, 1, 1], activation=tf.nn.relu, kernel_summary=False):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                weight_size = (kernel_size * kernel_size * input_features * output_features) / 256  # 4 bytes / 1024
                print("Conv %s : %s => %s (weight : %.02fKiB)" % (
                    name, str(symbolic_input.shape), str(output_features), weight_size))
            weights = tf.get_variable(
                "Kernel", shape=[kernel_size, kernel_size, input_features, output_features],
                initializer=tf.initializers.truncated_normal(stddev=0.1)) if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, kernel_size, input_features, output_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.get_variable("Bias", shape=[output_features], initializer=tf.constant_initializer(0.1))
            conv = tf.nn.conv2d(symbolic_input, weights, strides=strides, padding='SAME') + biases
            # network_output["summaries"].append(tf.summary.histogram('Kernel', weights))
            # network_output["summaries"].append(tf.summary.histogram('Biases', biases))
            # network_output["summaries"].append(tf.summary.histogram('Output', conv[0]))
            if batch_norm:
                conv = batch_normalization(
                    conv, is_training=is_training, batch_norm_training=batch_norm_training, name='Batch_Norm')
                # network_output["summaries"].append(tf.summary.histogram('PostBatchNorm', conv[1]))
            elif inst_norm:
                conv = tf.contrib.layers.instance_norm(conv, scope="Inst_Norm")

            if activation:
                conv = activation(conv)
                # network_output["summaries"].append(tf.summary.histogram('Activation', conv[1]))
            return conv

    def depthwise_conv(symbolic_input, output_features, name="Conv", is_training=False, batch_norm=True,
                       kernel_size=3, strides=[1, 1, 1, 1], activation=tf.nn.relu, kernel_summary=False):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                weight_size = (kernel_size * kernel_size * output_features) / 256  # 4 bytes / 1024
                print("Depth : %s : %s => %s (weight : %.02fKiB)" % (
                    name, str(symbolic_input.shape), str(output_features), weight_size))
            weights = tf.get_variable(
                "Kernel", shape=[kernel_size, kernel_size, output_features, 1],
                initializer=tf.initializers.truncated_normal(stddev=0.1)) if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, kernel_size, output_features, 1],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.get_variable("Bias", shape=[output_features], initializer=tf.constant_initializer(0.1))
            conv = tf.nn.depthwise_conv2d(
                symbolic_input, weights, strides=strides, padding='SAME', data_format='NHWC') + biases
            # network_output["summaries"].append(tf.summary.histogram('Kernel', weights))
            # network_output["summaries"].append(tf.summary.histogram('Biases', biases))
            # network_output["summaries"].append(tf.summary.histogram('Output', conv))
            if batch_norm:
                conv = batch_normalization(
                    conv, is_training=is_training, batch_norm_training=batch_norm_training, name='Batch_Norm')
                # network_output["summaries"].append(tf.summary.histogram('PostBatchNorm', conv))

            if activation:
                conv = activation(conv)
                # network_output["summaries"].append(tf.summary.histogram('Activation', conv))
            return conv

    def smart_convolution(symbolic_input, input_features, output_features, name="SmartConv", is_training=False,
                          batch_norm=True, inst_norm=False, kernel_size=3, strides=[1, 1, 1, 1], activation=tf.nn.relu,
                          kernel_summary=False):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                print(name)
            conv = convolution(
                symbolic_input, input_features, output_features, name="%dx%d" % (kernel_size, kernel_size),
                is_training=is_training, batch_norm=batch_norm, inst_norm=inst_norm, kernel_size=kernel_size,
                strides=strides, activation=activation, kernel_summary=kernel_summary)
            conv = depthwise_conv(
                conv, output_features, name="1x1", is_training=is_training, batch_norm=batch_norm,
                kernel_size=1, activation=activation)
            return conv

    def deconvolution(symbolic_input, input_features, output_features=None, output_shape=None, name="DeConv",
                      is_training=False, batch_norm=True, inst_norm=False,
                      kernel_size=3, strides=[1, 1, 1, 1], activation=tf.nn.relu):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if not output_shape:
                output_shape = symbolic_input.get_shape().as_list()  # [?, 32, 32, output_features]
                output_shape[0] = tf.shape(symbolic_input)[0]
                output_shape[1] *= strides[1]
                output_shape[2] *= strides[2]
                output_shape[3] = output_features
            else:
                output_shape[0] = tf.shape(symbolic_input)[0]
                output_features = output_shape[3]
            if __debug__:
                weight_size = (kernel_size * kernel_size * input_features * output_features) / 256  # 4 bytes / 1024
                print("%s : %s => %s (weight : %.02fKiB)" % (
                    name, str(symbolic_input.shape), str(output_shape[1:]), weight_size))
            weights = tf.get_variable(
                "Kernel", shape=[kernel_size, kernel_size, output_features, input_features],
                initializer=tf.initializers.truncated_normal(stddev=0.1)) if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, kernel_size, output_features, input_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.get_variable("Bias", shape=[output_features], initializer=tf.constant_initializer(0.1))
            deconv = tf.nn.conv2d_transpose(symbolic_input, weights, output_shape, strides, padding='SAME') + biases
            # network_output["summaries"].append(tf.summary.histogram('Kernel', weights))
            # network_output["summaries"].append(tf.summary.histogram('Biases', biases))
            # network_output["summaries"].append(tf.summary.histogram('Output', deconv))

            if batch_norm:
                deconv = batch_normalization(
                    deconv, is_training=is_training, batch_norm_training=batch_norm_training, name='Batch_Norm')
                # network_output["summaries"].append(tf.summary.histogram('PostBatchNorm', deconv))
            elif inst_norm:
                deconv = tf.contrib.layers.instance_norm(deconv, scope="Inst_Norm")

            if activation:
                deconv = activation(deconv)
                # network_output["summaries"].append(tf.summary.histogram('Activation', deconv))
            return deconv

    def smart_deconvolution(symbolic_input, input_features, output_features=None, output_shape=None, name="SmartDeConv",
                            is_training=False, batch_norm=True, inst_norm=False, kernel_size=3, strides=[1, 1, 1, 1],
                            activation=tf.nn.relu, kernel_summary=False):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                print(name)
            deconv = deconvolution(
                symbolic_input, input_features, output_features=output_features, output_shape=output_shape,
                name="%dx%d" % (kernel_size, kernel_size), is_training=is_training, batch_norm=batch_norm,
                inst_norm=inst_norm, kernel_size=kernel_size, strides=strides, activation=activation)
            if not output_features:
                output_features = output_shape[3]
            deconv = depthwise_conv(
                deconv, output_features, name="1x1", is_training=is_training, batch_norm=batch_norm,
                kernel_size=1, activation=activation)
            return deconv

    def fully_connected(symbolic_input, input_dimmension, output_dimmension, name="FC", is_training=False,
                        batch_norm=True, inst_norm=False, activation=tf.nn.relu):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                weight_size = (input_dimmension * output_dimmension) / 256  # 4 bytes / 1024
                print("%s : %s => %s (weight : %.02fKiB)" % (
                    name, str(symbolic_input.shape), str(output_dimmension), weight_size))
            weights = tf.get_variable(
                "Weight", shape=[input_dimmension, output_dimmension],
                initializer=tf.initializers.truncated_normal(stddev=0.1)) if is_training else (
                    tf.get_variable('Weight', shape=[input_dimmension, output_dimmension],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.get_variable("Bias", shape=[output_dimmension], initializer=tf.constant_initializer(0.1))
            fc = tf.matmul(symbolic_input, weights) + biases
            # network_output["summaries"].append(tf.summary.histogram('Weights', weights))
            # network_output["summaries"].append(tf.summary.histogram('Biases', biases))
            # network_output["summaries"].append(tf.summary.histogram('Output', fc))
            if batch_norm:
                fc = batch_normalization(
                    fc, is_training=is_training, batch_norm_training=batch_norm_training, name='Batch_Norm')
            elif inst_norm:
                fc = tf.contrib.layers.instance_norm(fc, scope="Inst_Norm")

            # network_output["summaries"].append(tf.summary.histogram('PostBatchNorm', fc))
            if activation:
                fc = activation(fc)
                # network_output["summaries"].append(tf.summary.histogram('Activation', fc))
            return fc

    def skip_connection(symbolic_input, skipped_input, output_dimmension, is_training=False, batch_norm=True,
                        name="SkipCon"):
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            if __debug__:
                print(name)
            skip_input = tf.concat([symbolic_input, skipped_input], axis=-1)
            input_shape = skip_input.get_shape().as_list()
            return convolution(
                skip_input, input_shape[-1], output_dimmension, name="1x1", is_training=is_training,
                batch_norm=batch_norm, kernel_size=1)

    def discriminator(discriminator_input, conv_shapes=[]):
        with tf.variable_scope("Discriminator"):
            if __debug__:
                print("\nDiscriminator")
            previous_conv_feature = discriminator_input_shape[3]
            discriminator_output = discriminator_input
            conv_iterator = 1
            for conv_feature in ModelConfig.D_CONV_FEATURES:
                discriminator_output = convolution(
                    discriminator_output, previous_conv_feature, conv_feature, batch_norm=False, inst_norm=False,
                    is_training=is_training, kernel_size=ModelConfig.D_KERNEL_SIZE_1, strides=[1, 2, 2, 1],
                    name="Conv%d" % conv_iterator)
                previous_conv_feature = conv_feature
                conv_shapes.append(discriminator_output.shape.as_list())
                conv_iterator += 1

            flat_dimmension = (conv_shapes[-1][1] * conv_shapes[-1][2] * conv_shapes[-1][3])
            discriminator_output = tf.reshape(discriminator_output, [-1, flat_dimmension])

            discriminator_output = fully_connected(
                discriminator_output, flat_dimmension, ModelConfig.D_FC1_NEURONS, is_training=is_training,
                batch_norm=False, inst_norm=False, name="FC1")
            return fully_connected(
                discriminator_output, ModelConfig.D_FC1_NEURONS, 1, is_training=is_training, activation=tf.nn.sigmoid,
                batch_norm=False, name="FC2")

    def generator(generator_input, conv_shapes=[]):
        with tf.variable_scope("Generator"):
            if __debug__:
                    print("\nGenerator")

            generator_output = tf.reshape(
                generator_input, [-1, conv_shapes[0][1], conv_shapes[0][2], conv_shapes[0][3]])

            previous_feature_count = conv_shapes[0][3]
            layer_iterator = 1
            for conv_shape in conv_shapes[1:-1]:
                generator_output = deconvolution(
                    generator_output, previous_feature_count, output_shape=conv_shape, is_training=is_training,
                    batch_norm=True,
                    kernel_size=ModelConfig.G_KERNEL_SIZE_1, strides=[1, 2, 2, 1], name="DeConv%d" % layer_iterator)
                previous_feature_count = conv_shape[-1]
                layer_iterator += 1

            generator_output = deconvolution(
                generator_output, previous_feature_count, conv_shapes[-1][-1],
                is_training=is_training, activation=tf.nn.tanh, batch_norm=False,
                kernel_size=ModelConfig.G_KERNEL_SIZE_2, strides=[1, 2, 2, 1], name="DeConv%d" % layer_iterator)
            return generator_output

    network_output = {}
    network_output["summaries"] = []
    generator_input_shape = generator_input.get_shape().as_list()
    discriminator_input_shape = discriminator_input.get_shape().as_list()

    discriminator_output = discriminator(discriminator_input)
    network_output["discriminator"] = discriminator_output

    conv_shapes = [
        [None, 2, 2, int(generator_input_shape[1] / 4)],
        [None, 4, 4, ModelConfig.G_DECONV1_FEATURES],
        [None, 7, 7, ModelConfig.G_DECONV2_FEATURES],
        [None, 14, 14, ModelConfig.G_DECONV3_FEATURES],
        discriminator_input_shape
    ]
    generator_output = generator(generator_input, conv_shapes)
    network_output["generator"] = generator_output

    discriminator_output = discriminator(generator_output)
    network_output["generator_discriminator"] = discriminator_output

    return network_output
