# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class ModelConfig(type):
    D_KERNEL_SIZE_1 = 3
    D_KERNEL_SIZE_2 = 5

    D_CONV_FEATURES = [8, 16]
    D_FC1_NEURONS = 100

    G_KERNEL_SIZE_1 = 3
    G_KERNEL_SIZE_2 = 5

    G_DECONV1_FEATURES = 64
    G_DECONV2_FEATURES = 32
    G_DECONV3_FEATURES = 16
