# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import json
import os
import resource
import shutil
import subprocess
import time

import numpy as np
import tensorflow as tf

from model import inference, ModelConfig
from train_config import TrainConfig


def save_mnist_data(save_path):
    """Retrieve mnist data and save the numpy array at the given path"""
    from tensorflow.examples.tutorials.mnist import input_data

    mnist_folder = "MNIST_data"
    mnist_data = input_data.read_data_sets(mnist_folder, one_hot=True)
    shutil.rmtree(mnist_folder)
    np.save(save_path, mnist_data.train._images)


def train():
    """..."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', help='Folder to save checkpoint and event')
    args = parser.parse_args()

    if not os.path.exists(TrainConfig.TRAIN_DATA_FILE):
        save_mnist_data(TrainConfig.TRAIN_DATA_FILE)
    train_images = np.load(TrainConfig.TRAIN_DATA_FILE)

    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    with tf.Session(config=session_config).as_default() as sess:
        # Setting the variables
        generator_input = tf.placeholder(
            tf.float32,
            shape=[None, TrainConfig.INPUT_SHAPE[0]],
            name="generator_input")
        discriminator_input = tf.placeholder(
            tf.float32,
            shape=[None, TrainConfig.OUTPUT_SHAPE[0] * TrainConfig.OUTPUT_SHAPE[1] * TrainConfig.OUTPUT_SHAPE[2]],
            name="discriminator_input")
        batch_norm_training = tf.placeholder(tf.bool, shape=[], name="batch_norm_training")

        reshape_input = tf.reshape(
                discriminator_input,
                [-1, TrainConfig.OUTPUT_SHAPE[0], TrainConfig.OUTPUT_SHAPE[1], TrainConfig.OUTPUT_SHAPE[2]])

        train_summaries = []
        image_summaries = []

        with tf.variable_scope("Normalization"):
            normalized_input = (reshape_input * 2) - 1

        with tf.variable_scope("Network"):
            network_output = inference(generator_input, normalized_input, True, batch_norm_training)
            discriminator_op = network_output["discriminator"]
            generator_op = network_output["generator"]
            generator_discriminator_op = network_output["generator_discriminator"]

        image_summaries.append(tf.summary.image(
            "generator_output", generator_op, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
        # image_summaries.append(tf.summary.image(
        #     "normalized_input", normalized_input, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
        # train_summaries.append(tf.summary.histogram("generator_output", generator_op))
        # train_summaries.append(tf.summary.histogram("discriminator_input", discriminator_input))

        # -------- Train and Evaluate the Mode --------

        print("\nInitializing trainer...", end="\r")
        with tf.variable_scope("Train"):
            step = tf.Variable(0, trainable=False, name="Step")

            # if TrainConfig.LABEL_SMOOTHING:
            #     label_smoothing = tf.train.exponential_decay(
            #         TrainConfig.LABEL_SMOOTHING, step, 300, 0.99)
            #     train_summaries.append(tf.summary.scalar('label_smoothing', label_smoothing))
            #     real_label = tf.ones_like(discriminator_op) * (1 - label_smoothing)
            # else:
            #     real_label = tf.ones_like(discriminator_op)
            # fake_label = tf.zeros_like(generator_discriminator_op)

            # learning_rate = tf.train.exponential_decay(
            #     TrainConfig.D_LEARNING_RATE, step, 100, 0.995)
            # train_summaries.append(tf.summary.scalar('learning_rate', learning_rate))
            # trainer = tf.train.MomentumOptimizer(learning_rate, 0.5)
            # trainer = tf.train.AdamOptimizer(TrainConfig.D_LEARNING_RATE)

            with tf.variable_scope("Discriminator"):
                with tf.variable_scope("loss"):
                    d_loss_real = tf.reduce_mean(-tf.log(discriminator_op))
                    d_loss_fake = tf.reduce_mean(-tf.log(1.0 - generator_discriminator_op))
                    if TrainConfig.LOSS_SMOOTHING:
                        loss_smooth = tf.train.exponential_decay(TrainConfig.LOSS_SMOOTHING, step, 100, 0.99)
                        discriminator_loss_op = (d_loss_real * (1 - loss_smooth)) +  d_loss_fake
                        train_summaries.append(tf.summary.scalar('loss_smooth', loss_smooth))
                    else:
                        discriminator_loss_op = d_loss_real + d_loss_fake
                    # d_loss_real = tf.reduce_mean(
                    #     tf.nn.sigmoid_cross_entropy_with_logits(
                    #         logits=discriminator_op, labels=real_label))
                    # d_loss_fake = tf.reduce_mean(
                    #     tf.nn.sigmoid_cross_entropy_with_logits(
                    #         logits=generator_discriminator_op, labels=fake_label))
                    # discriminator_loss_op = d_loss_real + d_loss_fake
                train_summaries.append(tf.summary.scalar('loss', discriminator_loss_op))
                train_summaries.append(tf.summary.scalar("loss_fake", d_loss_fake))
                train_summaries.append(tf.summary.scalar("loss_real", d_loss_real))

                with tf.variable_scope("optimizer"):
                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "Network/Discriminator")
                    if TrainConfig.D_LEARNING_RATE_DECAY:
                        learning_rate = tf.train.exponential_decay(
                            TrainConfig.D_LEARNING_RATE, step, 100, TrainConfig.D_LEARNING_RATE_DECAY)
                        train_summaries.append(tf.summary.scalar('learning_rate', learning_rate))
                        trainer = tf.train.MomentumOptimizer(learning_rate, 0.5)
                    else:
                        trainer = tf.train.MomentumOptimizer(TrainConfig.D_LEARNING_RATE, 0.5)
                    # trainer = tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE)
                    discriminator_train_op = trainer.minimize(
                        discriminator_loss_op, var_list=var_list, global_step=step)

            with tf.variable_scope("Generator"):
                with tf.variable_scope("loss"):
                    generator_loss_op = tf.reduce_mean(-tf.log(generator_discriminator_op))
                    # generator_loss_op = tf.reduce_mean(
                    #     tf.nn.sigmoid_cross_entropy_with_logits(
                    #         logits=generator_discriminator_op, labels=real_label))
                train_summaries.append(tf.summary.scalar('loss', generator_loss_op))

                with tf.variable_scope("optimizer"):
                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "Network/Generator")
                    if TrainConfig.G_LEARNING_RATE_DECAY:
                        learning_rate = tf.train.exponential_decay(
                            TrainConfig.G_LEARNING_RATE, step, 100, TrainConfig.G_LEARNING_RATE_DECAY)
                        train_summaries.append(tf.summary.scalar('learning_rate', learning_rate))
                        trainer = tf.train.MomentumOptimizer(learning_rate, 0.5)
                    else:
                        trainer = tf.train.MomentumOptimizer(TrainConfig.G_LEARNING_RATE, 0.5)
                    # trainer = tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE)
                    generator_train_op = trainer.minimize(
                        generator_loss_op, var_list=var_list, global_step=step)

        print("Initializing train... Done")

        print("Initializing variables...", end="\r")
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        print("Initializing variables... Done")

        # Training loop

        train_sample_count = len(train_images)
        step_per_epoch = int(train_sample_count / TrainConfig.BATCH_SIZE) * 2
        if (step_per_epoch * TrainConfig.BATCH_SIZE) < train_sample_count:
            step_per_epoch += 1

        print("Initializing saver...", end="\r")
        if args.checkpoint:
            if os.path.exists(args.checkpoint):
                shutil.rmtree(args.checkpoint)
            os.makedirs(args.checkpoint)

            # Saving the configuration as JSON files
            with open(os.path.join(args.checkpoint, "train_config.json"), "w") as config_file:
                config_dict = {}
                for key in TrainConfig.__dict__:
                    if key[0] != "_":
                        config_dict[key] = getattr(TrainConfig, key)
                config_file.write(json.dumps(config_dict, indent=3))
            with open(os.path.join(args.checkpoint, "model_config.json"), "w") as config_file:
                config_dict = {}
                for key in ModelConfig.__dict__:
                    if key[0] != "_":
                        config_dict[key] = getattr(ModelConfig, key)
                config_file.write(json.dumps(config_dict, indent=3))

            summary_op = tf.summary.merge(train_summaries + network_output["summaries"])
            image_op = tf.summary.merge(image_summaries)
            train_writer = tf.summary.FileWriter(os.path.join(args.checkpoint, "train"), sess.graph, flush_secs=30)
            # tf.all_variables() tf.global_variables(scope="Network") or tf.trainable_variables(scope="Network")
            saver = tf.train.Saver(tf.global_variables(scope="Network"), max_to_keep=TrainConfig.MAX_CHECKPOINT)
        else:
            train_writer = None
        print("Initializing saver... Done")

        def save_summary(batch_norm_train):
            if train_writer:
                summary = sess.run(
                    summary_op,
                    feed_dict={
                        generator_input: batch_noises,
                        discriminator_input: batch_images,
                        batch_norm_training: batch_norm_train})
                train_writer.add_summary(summary, global_step=global_step)
            saver.save(sess, os.path.join(args.checkpoint, "checkpoint"), global_step=global_step)

        print("\nTraining... (%d samples, %d steps per epoch)" % (train_sample_count, step_per_epoch))

        train_start_time = time.time()
        benchmark_time = time.time()
        benchmark_step = 0
        index_list = np.arange(train_sample_count)

        epoch = 0
        batch_iterator = 0
        speed = 0

        batch_images = []
        discriminator_steps = TrainConfig.DISCRIMINATOR_STEPS
        try:
            # from tensorflow.python.client import timeline
            global_step = 0
            batch_norm_train = True
            saver.save(sess, os.path.join(args.checkpoint, "checkpoint_0"))
            print("\nEpoch %d" % epoch)

            while global_step <= TrainConfig.MAX_STEPS:
                # if TrainConfig.BATCH_NORM_TRAIN_START and epoch > TrainConfig.BATCH_NORM_TRAIN_START:
                #     batch_norm_train = not batch_norm_train
                if global_step % 10 == 0:
                    if benchmark_step > 1:
                        speed = benchmark_step / (time.time() - benchmark_time)
                    print("Step %d, %0.02f steps/s, %0.02f input/sec" % (
                        global_step, speed, speed * TrainConfig.BATCH_SIZE * discriminator_steps), end="\r")

                batch_noises = np.random.normal(
                    -1, 1, size=[TrainConfig.BATCH_SIZE, TrainConfig.INPUT_SHAPE[0]])
                for _ in range(discriminator_steps):
                    batch_images = []
                    for batch_build_iterator in range(TrainConfig.BATCH_SIZE):
                        batch_images.append(train_images[index_list[batch_iterator + batch_build_iterator]])
                        batch_iterator += 1
                        if batch_iterator + batch_build_iterator >= train_sample_count - TrainConfig.BATCH_SIZE:
                            epoch += 1
                            print("\nEpoch %d" % epoch)
                            np.random.shuffle(index_list)
                            batch_iterator = 0
                    sess.run(
                        discriminator_train_op,
                        feed_dict={
                            generator_input: batch_noises,
                            discriminator_input: batch_images,
                            batch_norm_training: batch_norm_train})
                sess.run(
                    generator_train_op,
                    feed_dict={
                        generator_input: batch_noises,
                        discriminator_input: batch_images,
                        batch_norm_training: batch_norm_train})

                if global_step % int(TrainConfig.STEPS_BEFORE_SUMMARIES / discriminator_steps) == 0:
                    save_summary(batch_norm_train)
                    benchmark_time = time.time()
                    benchmark_step = 0
                    # test_accuracy()
                if global_step % int(TrainConfig.STEPS_BEFORE_IMAGES / discriminator_steps) == 0:
                    images = sess.run(
                        image_op,
                        feed_dict={
                            generator_input: batch_noises,
                            discriminator_input: batch_images,
                            batch_norm_training: batch_norm_train})
                    train_writer.add_summary(images, global_step=global_step)
                    # train_writer.flush()
                if global_step and TrainConfig.DISCRIMINATOR_STEPS_DECAY and (
                        discriminator_steps > 1 and global_step % TrainConfig.DISCRIMINATOR_STEPS_DECAY == 0):
                    discriminator_steps -= 1
                global_step += 1
                benchmark_step += 1
        except KeyboardInterrupt:
            pass
        save_summary(batch_norm_train)

        train_stop_time = time.time()

        train_memory_peak = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        train_gpu_memory = subprocess.check_output(
            "nvidia-smi --query-gpu=memory.used --format=csv,noheader", shell=True).decode()

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            train_gpu_memory = train_gpu_memory.split("\n")[int(os.environ["CUDA_VISIBLE_DEVICES"])]
        else:
            train_gpu_memory = " ".join(train_gpu_memory.split("\n"))

        result = "Training time : %gs\n\tRAM peak : %g MB\n\tVRAM usage : %s" % (
            train_stop_time - train_start_time,
            int(train_memory_peak / 1024),
            train_gpu_memory)
        print(result)
        # with open("output.txt", "w") as output_file:
        #     output_file.write(result)


if __name__ == '__main__':
    train()
