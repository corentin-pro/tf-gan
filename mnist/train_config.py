# coding: utf-8


class TrainConfig(type):
    INPUT_SHAPE = [100]
    OUTPUT_SHAPE = [28, 28, 1]

    BATCH_SIZE = 32
    MAX_STEPS = 30000

    STEPS_BEFORE_SUMMARIES = 200
    STEPS_BEFORE_IMAGES = 200
    IMAGE_MAX_OUTPUT = 8

    MAX_CHECKPOINT = 3

    D_LEARNING_RATE = 0.005
    D_LEARNING_RATE_DECAY = None
    G_LEARNING_RATE = 0.005
    G_LEARNING_RATE_DECAY = None

    DISCRIMINATOR_STEPS = 1  # this will devide summary/image step to compensate for the speed loss
    DISCRIMINATOR_STEPS_DECAY = None  # decreases DISCRIMINATOR_STEPS after given step count

    LABEL_SMOOTHING = None
    LOSS_SMOOTHING = None

    TRAIN_DATA_FILE = "./mnist_train_images.npy"
